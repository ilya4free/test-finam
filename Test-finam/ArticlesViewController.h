//
//  ArticlesViewController.h
//  Test-finam
//
//  Created by Илья on 23/03/2019.
//  Copyright © 2019 Илья. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SearchViewController;

NS_ASSUME_NONNULL_BEGIN

@interface ArticlesViewController : UITableViewController <UISearchResultsUpdating>{
	int	page;
	BOOL	downloadInProgress;
	NSMutableArray	<NSDictionary *>	*items;
	IBOutlet	UIActivityIndicatorView	*activityIndicator;
	UISearchController	*searchController;
	SearchViewController	*searchViewController;
}

@property (nonatomic, weak)	NSDictionary	*source;

@end

NS_ASSUME_NONNULL_END
