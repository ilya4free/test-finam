//
//  ListViewController.m
//  Test-finam
//
//  Created by Илья on 23/03/2019.
//  Copyright © 2019 Илья. All rights reserved.
//

#import "SourcesViewController.h"
#import "ArticlesViewController.h"
#import "Settings.h"

@interface SourcesViewController ()

@end

@implementation SourcesViewController


static	NSString *cellId	=	@"CellId";

- (void)viewDidLoad {
    [super viewDidLoad];
	
	items	=	[NSMutableArray array];
	
	self.navigationItem.title	=	@"Sources";
	self.navigationItem.rightBarButtonItem	=	[[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
	[activityIndicator startAnimating];
	self.tableView.hidden	=	YES;
	NSURL	*url	=	[NSURL URLWithString:[NSString stringWithFormat:@"https://newsapi.org/v2/sources?language=en&apiKey=%@",kAPI_KEY]];
	[[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
		if ( data ) {
			NSError	*error;
			NSDictionary	*json	=	[NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
			if ( json ) {
				static NSString	*jsonDataKey	=	@"sources";
				if ( [[json valueForKey:@"status"] isEqualToString:kRESPONSE_STATUS_OK] ) {
					if ( [json valueForKey:jsonDataKey]  &&  [[json valueForKey:jsonDataKey] isKindOfClass:[NSArray class]] ) {
						for ( NSDictionary *dictionary in [json valueForKey:jsonDataKey] ) {
							[self->items addObject:dictionary];
						}
					}
				}else{
					NSLog(@"ERROR loading: %@, %@", [json valueForKey:@"code"],[json valueForKey:@"message"] );
				}
			}
		}
		dispatch_async( dispatch_get_main_queue(), ^{
			self.tableView.hidden	=	NO;
			[self->activityIndicator stopAnimating];
			[self.tableView reloadData];
		});
	}] resume];
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
	if ( !cell ) {
		cell	=	[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
	}
	NSDictionary	*item	=	items[indexPath.row];
	cell.textLabel.text	=	item[@"name"];
	cell.detailTextLabel.text	=	item[@"description"];
	cell.accessoryType	=	UITableViewCellAccessoryDetailDisclosureButton;
	return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	ArticlesViewController	*nextController	=	[[ArticlesViewController alloc] init];
	nextController.source	=	items[indexPath.row];
	[self.navigationController pushViewController:nextController animated:YES];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
	NSDictionary	*item	=	items[indexPath.row];
	NSURL	*url	=	[NSURL URLWithString:item[@"url"]];
	if ( [[UIApplication sharedApplication] canOpenURL:url] ) {
		[[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {}];
	}
}

@end
