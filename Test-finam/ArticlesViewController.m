//
//  ArticlesViewController.m
//  Test-finam
//
//  Created by Илья on 23/03/2019.
//  Copyright © 2019 Илья. All rights reserved.
//

#import "ArticlesViewController.h"
#import "SearchViewController.h"
#import "MoreViewController.h"
#import "Settings.h"

@interface ArticlesViewController ()

@end

@implementation ArticlesViewController

@synthesize source;

static	NSString *cellId	=	@"CellId";
static	NSInteger	pageSize	=	20;

- (void)viewDidLoad {
    [super viewDidLoad];
	page	=	1;
	items	=	[NSMutableArray array];
	self.navigationItem.title	=	@"Articles";
	
	self.navigationItem.rightBarButtonItem	=	[[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
	
	UIRefreshControl	*refreshControl	=	[[UIRefreshControl alloc] init];
	[refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
	self.tableView.refreshControl	=	refreshControl;
	
	searchViewController	=	[[SearchViewController alloc] init];
	searchViewController.navigationController	=	self.navigationController;
	searchController	=	[[UISearchController alloc] initWithSearchResultsController:searchViewController];
	searchController.searchResultsUpdater	=	self;
	searchController.searchBar.placeholder	=	[NSString stringWithFormat:@"Search in %@",self.source[@"name"]];
	self.tableView.tableHeaderView	=	searchController.searchBar;
	self.definesPresentationContext		=	YES;
	
	downloadInProgress	=	YES;
	NSURL	*url	=	[NSURL URLWithString:[NSString stringWithFormat:@"https://newsapi.org/v2/everything?language=en&sources=%@&apiKey=%@&pageSize=%ld&page=1",self.source[@"id"],kAPI_KEY,pageSize]];
	NSLog(@"DID APP %@",url);
	[items removeAllObjects];
	[[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
		if ( data ) {
			NSError	*error;
			NSDictionary	*json	=	[NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
			if ( json ) {
				static NSString	*jsonDataKey	=	@"articles";
				if ( [[json valueForKey:@"status"] isEqualToString:kRESPONSE_STATUS_OK] ) {
					if ( [json valueForKey:jsonDataKey]  &&  [[json valueForKey:jsonDataKey] isKindOfClass:[NSArray class]] ) {
						for ( NSDictionary *dictionary in [json valueForKey:jsonDataKey] ) {
							[self->items addObject:dictionary];
						}
					}
				}else{
					NSLog(@"ERROR loading: %@, %@", [json valueForKey:@"code"],[json valueForKey:@"message"] );
				}
			}
		}
		dispatch_async( dispatch_get_main_queue(), ^{
			self.tableView.hidden	=	NO;
			[self->activityIndicator stopAnimating];
			[self.tableView reloadData];
			self->downloadInProgress	=	NO;
		});
	}] resume];
	
}

- (void)refresh:(UIRefreshControl *)refreshControl{
	NSURL	*url	=	[NSURL URLWithString:[NSString stringWithFormat:@"https://newsapi.org/v2/everything?language=en&sources=%@&apiKey=%@",self.source[@"id"],kAPI_KEY]];
	if ( !downloadInProgress ) {
		downloadInProgress	=	YES;
		NSLog(@"%@",url);
		[[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
			if ( data ) {
				NSError	*error;
				NSDictionary	*json	=	[NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
				if ( json ) {
					static NSString	*jsonDataKey	=	@"articles";
					if ( [[json valueForKey:@"status"] isEqualToString:kRESPONSE_STATUS_OK] ) {
						if ( [json valueForKey:jsonDataKey]  &&  [[json valueForKey:jsonDataKey] isKindOfClass:[NSArray class]] ) {
							[self->items removeAllObjects];
							for ( NSDictionary *dictionary in [json valueForKey:jsonDataKey] ) {
								[self->items addObject:dictionary];
							}
						}
					}else{
						NSLog(@"ERROR loading: %@, %@", [json valueForKey:@"code"],[json valueForKey:@"message"] );
					}
				}
			}
			dispatch_async( dispatch_get_main_queue(), ^{
				self.tableView.hidden	=	NO;
				[self->activityIndicator stopAnimating];
				[self.tableView reloadData];
				self->downloadInProgress	=	NO;
			});
		}] resume];
		[refreshControl endRefreshing];
	}
}

- (void)viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
	if ( !cell ) {
		cell	=	[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
	}
	cell.textLabel.text	=	items[indexPath.row][@"title"];
	cell.detailTextLabel.text	=	items[indexPath.row][@"description"];
	cell.detailTextLabel.textColor	=	[UIColor grayColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	MoreViewController	*controller	=	[[MoreViewController alloc] init];
	controller.article	=	items[indexPath.row];
	[self presentViewController:controller animated:YES completion:nil];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
	searchViewController.source	=	self.source;
	searchViewController.queryString	=	searchController.searchBar.text;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	if ( [scrollView isEqual:self.tableView]  &&  !downloadInProgress  &&  (page * pageSize) < 100 ) {
		downloadInProgress	=	YES;
		page	+=	1;
		NSURL	*url	=	[NSURL URLWithString:[NSString stringWithFormat:@"https://newsapi.org/v2/everything?language=en&sources=%@&apiKey=%@&pageSize=%ld&page=%d",self.source[@"id"],kAPI_KEY,pageSize,page]];
		NSLog(@"DID SCR %@",url);
		[[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
			if ( data ) {
				NSError	*error;
				NSDictionary	*json	=	[NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
				if ( json ) {
					static NSString	*jsonDataKey	=	@"articles";
					if ( [[json valueForKey:@"status"] isEqualToString:kRESPONSE_STATUS_OK] ) {
						if ( [json valueForKey:jsonDataKey]  &&  [[json valueForKey:jsonDataKey] isKindOfClass:[NSArray class]] ) {
							for ( NSDictionary *dictionary in [json valueForKey:jsonDataKey] ) {
								[self->items addObject:dictionary];
							}
						}
					}else{
						NSLog(@"ERROR loading: %@, %@", [json valueForKey:@"code"],[json valueForKey:@"message"] );
					}
				}
			}
			dispatch_async( dispatch_get_main_queue(), ^{
				self.tableView.hidden	=	NO;
				[self->activityIndicator stopAnimating];
				[self.tableView reloadData];
				self->downloadInProgress	=	NO;
			});
		}] resume];

	}
}

@end
