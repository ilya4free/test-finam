//
//  SearchViewController.m
//  Test-finam
//
//  Created by Илья on 23/03/2019.
//  Copyright © 2019 Илья. All rights reserved.
//

#import "SearchViewController.h"
#import "MoreViewController.h"
#import "Settings.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

static	NSString *cellId	=	@"CellId";

@synthesize  queryString, source;

- (void)viewDidLoad {
    [super viewDidLoad];
	items	=	[NSMutableArray array];
}


- (void)setQueryString:(NSString *)aQueryString{
	queryString	=	aQueryString;
	if ( queryString.length >= 2 ) {
		[items removeAllObjects];
		NSURL	*url	=	[NSURL URLWithString:[NSString stringWithFormat:@"https://newsapi.org/v2/everything?q=%@&language=en&sources=%@&apiKey=%@",self.queryString,self.source[@"id"],kAPI_KEY]];
		[[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
			if ( data ) {
				NSError	*error;
				NSDictionary	*json	=	[NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
				if ( json ) {
					static NSString	*jsonDataKey	=	@"articles";
					if ( [[json valueForKey:@"status"] isEqualToString:kRESPONSE_STATUS_OK] ) {
						if ( [json valueForKey:jsonDataKey]  &&  [[json valueForKey:jsonDataKey] isKindOfClass:[NSArray class]] ) {
							for ( NSDictionary *dictionary in [json valueForKey:jsonDataKey] ) {
								[self->items addObject:dictionary];
							}
						}
					}else{
						NSLog(@"ERROR loading: %@, %@", [json valueForKey:@"code"],[json valueForKey:@"message"] );
					}
				}
			}
			dispatch_async( dispatch_get_main_queue(), ^{
				self.tableView.hidden	=	NO;
				[self.tableView reloadData];
			});
		}] resume];
	}else{
		[items removeAllObjects];
		[self.tableView reloadData];
	}
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
	if ( !cell ) {
		cell	=	[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
	}
	cell.textLabel.text	=	items[indexPath.row][@"title"];
	cell.detailTextLabel.text	=	items[indexPath.row][@"description"];
	cell.detailTextLabel.textColor	=	[UIColor grayColor];
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	MoreViewController	*controller	=	[[MoreViewController alloc] init];
	controller.article	=	items[indexPath.row];
	[self presentViewController:controller animated:YES completion:nil];
}

@end
