//
//  MoreViewController.m
//  Test-finam
//
//  Created by Илья on 23/03/2019.
//  Copyright © 2019 Илья. All rights reserved.
//

#import "MoreViewController.h"

@interface MoreViewController ()

@end

@implementation MoreViewController

@synthesize article;

- (void)viewDidLoad {
    [super viewDidLoad];
	if ( article ) {
		if ( ![[article valueForKey:@"publishedAt"] isEqual:[NSNull null]] ) {
			publishAtLabel.text	=	[article valueForKey:@"publishedAt"];
		}else{
			publishAtLabel.text	=	@"";
		}
		if ( ![[article valueForKey:@"title"] isEqual:[NSNull null]] ) {
			titleLabel.text	=	[article valueForKey:@"title"];
		}else{
			titleLabel.text	=	@"";
		}
		if ( ![[article valueForKey:@"author"] isEqual:[NSNull null]] ) {
			authorLabel.text	=	[article valueForKey:@"author"];
		}else{
			authorLabel.text	=	@"";
		}
		if ( ![[article valueForKey:@"description"] isEqual:[NSNull null]] ) {
			descriptionLabel.text	=	[article valueForKey:@"description"];
		}else{
			descriptionLabel.text	=	@"";
		}
		if ( ![[article valueForKey:@"content"] isEqual:[NSNull null]] ) {
			contentLabel.text	=	[article valueForKey:@"content"];
		}else{
			contentLabel.text	=	@"";
		}
		
		if ( ![[article valueForKey:@"urlToImage"] isEqual:[NSNull null]] ) {
			[activityIndicator startAnimating];
			[[[NSURLSession sharedSession] downloadTaskWithURL:[NSURL URLWithString:article[@"urlToImage"]] completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
				dispatch_async( dispatch_get_main_queue(), ^{
					if ( !error && location ) {
						NSData	*data	=	[NSData dataWithContentsOfURL:location];
						if ( data ) {
							self->imageView.image	=	[UIImage imageWithData:data];
						}
						[self->activityIndicator stopAnimating];
					}
				});
			}] resume];
		}
		
	}else{
		publishAtLabel.hidden	=	YES;
		titleLabel.hidden	=	YES;
		authorLabel.hidden	=	YES;
		descriptionLabel.hidden	=	YES;
		contentLabel.hidden	=	YES;
		imageView.hidden	=	YES;
		urlButton.hidden	=	YES;
		
	}
}

- (IBAction)closeButtonPressed:(id)sender{
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)urlButtonPressed:(id)sender{
	NSURL	*url	=	[NSURL URLWithString:article[@"url"]];
	if ( [[UIApplication sharedApplication] canOpenURL:url] ) {
		[[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {}];
	}
}

@end
