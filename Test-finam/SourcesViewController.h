//
//  ListViewController.h
//  Test-finam
//
//  Created by Илья on 23/03/2019.
//  Copyright © 2019 Илья. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SourcesViewController : UITableViewController{
	NSMutableArray	<NSDictionary *>	*items;
	IBOutlet	UIActivityIndicatorView	*activityIndicator;
}

@end

NS_ASSUME_NONNULL_END
