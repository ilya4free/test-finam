//
//  SearchViewController.h
//  Test-finam
//
//  Created by Илья on 23/03/2019.
//  Copyright © 2019 Илья. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchViewController : UITableViewController{
	NSMutableArray	<NSDictionary *>	*items;
}

@property (nonatomic, weak)	NSDictionary	*source;
@property (nonatomic, weak)	NSString	*queryString;
@property (nonatomic, weak)	UINavigationController	*navigationController;

@end

NS_ASSUME_NONNULL_END
