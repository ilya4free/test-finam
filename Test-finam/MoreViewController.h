//
//  MoreViewController.h
//  Test-finam
//
//  Created by Илья on 23/03/2019.
//  Copyright © 2019 Илья. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MoreViewController : UIViewController{
	IBOutlet	UIActivityIndicatorView	*activityIndicator;
	IBOutlet	UILabel	*publishAtLabel;
	IBOutlet	UILabel	*titleLabel;
	IBOutlet	UILabel	*authorLabel;
	IBOutlet	UILabel	*descriptionLabel;
	IBOutlet	UILabel	*contentLabel;
	IBOutlet	UIImageView	*imageView;
	IBOutlet	UIButton	*urlButton;
}

@property (nonatomic, strong)	NSDictionary	*article;

@end

NS_ASSUME_NONNULL_END
